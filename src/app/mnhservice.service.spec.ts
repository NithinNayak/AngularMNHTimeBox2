import { TestBed, inject } from '@angular/core/testing';

import { MnhserviceService } from './mnhservice.service';

describe('MnhserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MnhserviceService]
    });
  });

  it('should be created', inject([MnhserviceService], (service: MnhserviceService) => {
    expect(service).toBeTruthy();
  }));
});
