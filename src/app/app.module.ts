import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import { MnhserviceService } from './mnhservice.service';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule
  ],
  providers: [MnhserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
