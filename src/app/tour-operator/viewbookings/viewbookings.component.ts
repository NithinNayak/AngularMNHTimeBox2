import { Component, OnInit } from '@angular/core';
import { MnhserviceService } from '../../mnhservice.service';
import swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewbookings',
  templateUrl: './viewbookings.component.html',
  styleUrls: ['./viewbookings.component.css']
})
export class ViewbookingsComponent implements OnInit {
  isSpinning:boolean;
  isDataPresent:boolean;
  isVerified:boolean;
  tourId:number=1;
  bookingDetails=[];
  constructor(private service:MnhserviceService,private router:Router) { }

  ngOnInit() {
    this.isDataPresent=true;
    this.isSpinning=true;
    this.isVerified=true;
    this.service.fetchBookings(this.tourId).subscribe(data=>{
      this.bookingDetails=data;
      if(this.bookingDetails.length==0)
      {
        this.isDataPresent=false;
        swal({
          title: 'There are no bookings at your place',
          text: "Sit back and relax till then!",
          type: 'error',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK!'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl('/touroperator/tourfront');
          }
        })
      }
      else if(this.bookingDetails[0].tourOpStatus=='pending')
      {
        this.isVerified=false;
        swal({
          title: 'Your account has not been verified yet?',
          text: "We will get back to you soon!",
          type: 'error',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK!'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl('/touroperator/tourfront');
          }
        })
      }
       this.isSpinning=false;
    });
  }
  sendRequestMail(i)
  {
    swal({
      position: 'center',
      type: 'success',
      title: 'Request has been sent',
      showConfirmButton: false,
      timer: 1500
    })
    alert(this.bookingDetails[i].email+" "+i);
    this.service.sendRequestMail(this.bookingDetails[i].email+"/"+this.tourId+"/"+this.bookingDetails[i].travellerName);
  }

}
