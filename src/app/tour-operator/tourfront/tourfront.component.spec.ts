import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourfrontComponent } from './tourfront.component';

describe('TourfrontComponent', () => {
  let component: TourfrontComponent;
  let fixture: ComponentFixture<TourfrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourfrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourfrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
