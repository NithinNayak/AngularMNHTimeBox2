import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddtariffplansComponent } from './addtariffplans/addtariffplans.component';
import { TourfrontComponent } from './tourfront/tourfront.component';
import { ViewbookingsComponent } from './viewbookings/viewbookings.component';



const routes: Routes = [

{path:'addtariffplans',component:AddtariffplansComponent},
    {path:'viewbookings',component:ViewbookingsComponent},
  {path:'tourfront',component:TourfrontComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TourOperatorRoutingModule { }
export const routingComponents=[AddtariffplansComponent,ViewbookingsComponent,TourfrontComponent];
