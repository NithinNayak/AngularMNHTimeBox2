import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtariffplansComponent } from './addtariffplans.component';

describe('AddtariffplansComponent', () => {
  let component: AddtariffplansComponent;
  let fixture: ComponentFixture<AddtariffplansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtariffplansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtariffplansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
