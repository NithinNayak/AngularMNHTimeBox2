import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import { MnhserviceService } from '../../mnhservice.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addtariffplans',
  templateUrl: './addtariffplans.component.html',
  styleUrls: ['./addtariffplans.component.css']
})
export class AddtariffplansComponent implements OnInit {
userForm:FormGroup;
CityStates=[];
cities=[];
j:number;
id:number=1;
  constructor(private service:MnhserviceService,private router:Router) { }

  ngOnInit() {
    this.userForm=new FormGroup({
      tourId:new FormControl(this.id),
      name:new FormControl('',[Validators.required,Validators.pattern('[A-Za-z][A-Za-z ]*')]),
      address:new FormControl('',[Validators.required,Validators.pattern('[A-Za-z][A-Za-z0-9#,/ ]*')]),
      state:new FormControl('',Validators.required),
      city:new FormControl('',Validators.required),
      vehicles:new FormControl('',[Validators.required,Validators.pattern('[1-9][0-9]*')]),
      maxTraveler:new FormControl('',[Validators.required,Validators.pattern('[1-9][0-9]*')]),
      cost:new FormControl('',[Validators.required,Validators.pattern('[1-9][0-9]*')])
    }),
    this.service.getData().subscribe(data=>{this.CityStates=data;

      console.log(this.CityStates[0].state);
      console.log(this.CityStates[0].cities[0]);
    })
}
getCityList(i)
{
  this.cities=this.CityStates[i].cities;
}
addPackage()
{
  swal({
    position: 'center',
    type: 'success',
    title: 'Successfully added the package',
    showConfirmButton: false,
    timer: 1500
  })
  
  this.service.addPackageDetails(this.userForm.value);
}
}
