import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TourOperatorRoutingModule,routingComponents } from './tour-operator-routing.module';
import { AddtariffplansComponent } from './addtariffplans/addtariffplans.component';
import { ViewbookingsComponent } from './viewbookings/viewbookings.component';
import { TourfrontComponent } from './tourfront/tourfront.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    CommonModule,
    TourOperatorRoutingModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatProgressSpinnerModule
  ],
  declarations: [ AddtariffplansComponent, ViewbookingsComponent, TourfrontComponent]
})
export class TourOperatorModule { }
