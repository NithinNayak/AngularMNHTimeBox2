import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MnhserviceService {
  private jsonurl="/assets/StateCity.json";
  private url="http://localhost:8080/MyNiceHome/";
  constructor(private http: HttpClient) { }
  getData():Observable<any[]>
  {
    return this.http.get<any[]>(this.jsonurl);
  }
  addPackageDetails(packageDetails)
  {
    this.http.post(this.url+'addPackageDetails',packageDetails).subscribe();
  }
  fetchBookings(id):Observable<any[]>
  {
    return this.http.post<any[]>(this.url+'fetchBookings',id);
  }
  sendRequestMail(emailBodyDetails)
  {
    this.http.post(this.url+'sendMailRequest',emailBodyDetails).subscribe();
  }
}
